import numpy as np
from astropy.coordinates import SkyCoord
from .SkyObject import SkyObject


class CoordinateGrid(SkyObject):
    """
    This class contains a grid of equatorial coordinates to plot it.
    """
    def __init__(self, location):
        """
        In the constructor a set of circles of equal ra and dec is computed
        """
        super().__init__(location)
        d_alpha = 15  # Step in alpha
        d_delta = 10  # Step in delta
        self.points_per_circle = 50

        ra_grid, dec_grid = [], []
        # Create circles of equal delta
        alpha = np.linspace(0, 360, self.points_per_circle)
        for delta in np.arange(89.9, location.lat.value-90, -d_delta):
            ra_grid.extend(alpha)
            dec_grid.extend(np.zeros_like(alpha) + delta)
            ra_grid.append(0)
            dec_grid.append(-90)
        # Create circles of equal alpha
        delta = np.linspace(location.lat.value-90, 89.9, self.points_per_circle)
        for alpha in np.arange(0, 360, d_alpha):
            ra_grid.extend(np.zeros_like(delta) + alpha)
            dec_grid.extend(delta)
            ra_grid.append(0)
            dec_grid.append(-90)
        self.equatorial_coords = SkyCoord(ra=ra_grid, dec=dec_grid, frame="fk5", unit="deg")

    def plot(self, ax):
        # Plot the grid circle by circle
        self.remove()
        zenith_distance = np.pi/2 - np.radians(self.horizonthal_coords.alt.deg)
        azimuth = np.radians(self.horizonthal_coords.az.deg) + np.pi
        bad_inds = np.where(zenith_distance > np.pi/2)
        x = zenith_distance * np.sin(azimuth)
        y = - zenith_distance * np.cos(azimuth)
        x[bad_inds] = None
        y[bad_inds] = None
        line_instance = ax.plot(x, y, markersize=1, color="0.75", linestyle="-")[0]
        self.plot_instances.append(line_instance)

        # Plot horizon
        zenith_distance = np.pi / 2
        azimuth = np.linspace(0, 2*np.pi, 50)
        x = zenith_distance * np.sin(azimuth)
        y = - zenith_distance * np.cos(azimuth)
        line_instance = ax.plot(x, y, "--", color="0.5")[0]
        self.plot_instances.append(line_instance)
