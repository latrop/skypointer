#! /usr/bin/env python

import os
import time
import threading
from PyQt5 import QtWidgets
from astropy.coordinates import EarthLocation
from astropy import units
from astropy.utils import iers
from .SkyPlot import SkyPlot
from .TimeControls import TimeControls
from .ObjectPanel import ObjectPanel
from .TelescopePanel import TelescopePanel
from .Telescope import Telescope
from .sidereal import SiderealClock
iers.conf.auto_download = False


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.is_alive = True
        # Site location
        self.site_latitude = 59.880412
        self.site_longitude = 29.824820
        self.site_height = 0.0
        self.site_location = EarthLocation(lat=self.site_latitude*units.deg,
                                           lon=self.site_longitude*units.deg,
                                           height=self.site_height*units.m)

        # Create telescope object
        self.telescope = Telescope(self)
        # Start sidereal clock
        self.sidereal_clock = SiderealClock(self)
        self.initUI()

    def initUI(self):
        self.setWindowTitle("SkyPointer")
        # Main window layout
        self.central_widget = QtWidgets.QWidget()
        self.setCentralWidget(self.central_widget)

        self.main_window_layout = QtWidgets.QHBoxLayout()
        # Left side of the window where the plot and some buttons are located
        self.v_box_left = QtWidgets.QVBoxLayout()
        # Sky plot
        self.sky_plot = SkyPlot(self)
        # Time controls
        self.time_controls = TimeControls(self)

        # Right panel layout
        self.v_box_right = QtWidgets.QVBoxLayout()
        self.object_panel = ObjectPanel(self)
        self.telescope_panel = TelescopePanel(self)

        # Connect time controls to the sky plotter
        self.time_controls.time_shift_button_pressed.connect(self.sky_plot.shift_time)
        # Connect the sky plot to the object panel
        self.sky_plot.selected_object_update.connect(self.object_panel.set_object_parameters)
        self.sky_plot.object_selected.connect(self.telescope_panel.set_target_coordinates)
        # Connect sidereal time clock to object panel
        self.sidereal_clock.time_update.connect(self.object_panel.set_sidereal_time)
        # Connect navigation panel to telescope
        self.telescope_panel.slew_signal.connect(self.telescope.go_to_target)
        self.telescope_panel.nav_stop_button.clicked.connect(self.telescope.halt)
        # self.telescope_panel.nav_NW_button.pressed.connect(lambda: self.telescope.guide("nw"))
        self.telescope_panel.nav_N_button.pressed.connect(lambda: self.telescope.guide("n"))
        # self.telescope_panel.nav_NE_button.pressed.connect(lambda: self.telescope.guide("ne"))
        self.telescope_panel.nav_W_button.pressed.connect(lambda: self.telescope.guide("e"))
        self.telescope_panel.nav_E_button.pressed.connect(lambda: self.telescope.guide("w"))
        # self.telescope_panel.nav_SW_button.pressed.connect(lambda: self.telescope.guide("sw"))
        self.telescope_panel.nav_S_button.pressed.connect(lambda: self.telescope.guide("s"))
        # self.telescope_panel.nav_SE_button.pressed.connect(lambda: self.telescope.guide("se"))
        # self.telescope_panel.nav_NW_button.released.connect(self.telescope.halt)
        self.telescope_panel.nav_N_button.released.connect(self.telescope.halt)
        # self.telescope_panel.nav_NE_button.released.connect(self.telescope.halt)
        self.telescope_panel.nav_W_button.released.connect(self.telescope.halt)
        self.telescope_panel.nav_E_button.released.connect(self.telescope.halt)
        # self.telescope_panel.nav_SW_button.released.connect(self.telescope.halt)
        self.telescope_panel.nav_S_button.released.connect(self.telescope.halt)
        # self.telescope_panel.nav_SE_button.released.connect(self.telescope.halt)

        # Connect telescope signals
        self.telescope.status_signal.connect(self.telescope_panel.set_status)
        self.telescope.new_coordinates.connect(self.on_new_coordinates_recieved)
        self.telescope.connected_signal.connect(self.on_telescope_connect)
        self.telescope.disconnected_signal.connect(self.on_telescope_disconnect)
        self.telescope.sleep_signal.connect(self.on_telescope_sleep)
        self.telescope.wake_signal.connect(self.on_telescope_wake)

        # Menubar
        # Create exit action
        exitAction = QtWidgets.QAction('&Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.close)
        menu_bar = self.menuBar()
        # File menu
        file_menu = menu_bar.addMenu('&File')
        file_menu.addAction(exitAction)
        # Telescope menu
        telescope_menu = menu_bar.addMenu('Telescope')
        # Connect telescope
        self.telescope_connect_action = QtWidgets.QAction("Connect", self)
        if os.name == "nt":
            self.telescope_connect_action.triggered.connect(self.telescope.connect)
        else:
            self.telescope_connect_action.triggered.connect(self.telescope.connect_dummy)
        self.telescope_connect_action.setEnabled(False)
        threading.Thread(target=self.enable_connect).start()
        telescope_menu.addAction(self.telescope_connect_action)
        # Disconnect telescope
        self.telescope_disconnect_action = QtWidgets.QAction("Disconnect", self)
        self.telescope_disconnect_action.triggered.connect(self.telescope.disconnect)
        self.telescope_disconnect_action.setEnabled(False)
        telescope_menu.addAction(self.telescope_disconnect_action)
        # Sleep telescope
        self.telescope_sleep_action = QtWidgets.QAction("Sleep", self)
        self.telescope_sleep_action.triggered.connect(self.telescope.sleep)
        self.telescope_sleep_action.setEnabled(False)
        telescope_menu.addAction(self.telescope_sleep_action)
        # Wake telescope
        self.telescope_wake_action = QtWidgets.QAction("Wake", self)
        self.telescope_wake_action.triggered.connect(self.telescope.wake)
        self.telescope_wake_action.setEnabled(False)
        telescope_menu.addAction(self.telescope_wake_action)
        # Park telescope
        self.telescope_park_action = QtWidgets.QAction("Park", self)
        self.telescope_park_action.triggered.connect(self.telescope.park)
        self.telescope_park_action.setEnabled(False)
        telescope_menu.addAction(self.telescope_park_action)
        # Alignment menu
        # align_menu = menu_bar.addMenu("Alignment")
        # # Start alignment
        # self.start_align_action = QtWidgets.QAction("Begin")
        # self.start_align_action.setEnabled(False)
        # self.start_align_action.triggered.connect(self.telescope.start_alignment)
        # align_menu.addAction(self.start_align_action)
        # # Finish alignment
        # self.finish_align_action = QtWidgets.QAction("Finish")
        # self.finish_align_action.setEnabled(False)
        # self.finish_align_action.triggered.connect(self.telescope.finish_alignment)
        # align_menu.addAction(self.finish_align_action)
        # # Skip alignment
        # self.skip_align_action = QtWidgets.QAction("Skip")
        # self.skip_align_action.setEnabled(False)
        # self.skip_align_action.triggered.connect(self.telescope.skip_alignment)
        # align_menu.addAction(self.skip_align_action)

        # Add widgets and layouts
        self.v_box_left.addWidget(self.sky_plot)
        self.v_box_left.addWidget(self.time_controls)
        self.v_box_right.addWidget(self.object_panel)
        self.v_box_right.addWidget(self.telescope_panel)
        self.main_window_layout.addLayout(self.v_box_left)
        self.main_window_layout.addLayout(self.v_box_right)
        # self.main_window_layout.addStretch()
        self.centralWidget().setLayout(self.main_window_layout)
        self.show()

    def enable_connect(self):
        time.sleep(3)
        self.telescope_connect_action.setEnabled(True)

    def on_telescope_connect(self):
        """
        GUI actions to perform when the telescope is connected
        """
        self.telescope_panel.enable_controls()
        self.telescope_disconnect_action.setEnabled(True)
        self.telescope_sleep_action.setEnabled(True)
        self.telescope_park_action.setEnabled(True)
        self.telescope_wake_action.setEnabled(False)
        self.telescope_connect_action.setEnabled(False)
        # self.start_align_action.setEnabled(True)
        # self.finish_align_action.setEnabled(True)
        # self.skip_align_action.setEnabled(True)

    def on_telescope_disconnect(self):
        """
        GUI actions to perform when the telescope is disconnected
        """
        self.telescope_panel.disable_controls()
        self.telescope_disconnect_action.setEnabled(False)
        self.telescope_sleep_action.setEnabled(False)
        self.telescope_wake_action.setEnabled(False)
        self.telescope_park_action.setEnabled(False)
        self.telescope_connect_action.setEnabled(True)
        # self.start_align_action.setEnabled(False)
        # self.finish_align_action.setEnabled(False)
        # self.skip_align_action.setEnabled(False)

    def on_new_coordinates_recieved(self, new_coordinates):
        if self.telescope.aligned is True:
            self.telescope_panel.set_current_coordinates(new_coordinates)
            self.sky_plot.move_telescope(new_coordinates)

    def on_telescope_sleep(self):
        """
        GUI actions to perform when the telescope is put to sleep
        """
        self.telescope_panel.disable_controls()
        self.telescope_disconnect_action.setEnabled(False)
        self.telescope_sleep_action.setEnabled(False)
        self.telescope_wake_action.setEnabled(True)
        self.telescope_connect_action.setEnabled(False)
        self.telescope_park_action.setEnabled(False)
        # self.start_align_action.setEnabled(False)
        # self.finish_align_action.setEnabled(False)
        # self.skip_align_action.setEnabled(False)

    def on_telescope_wake(self):
        """
        GUI actions to perform when the telescope is waken up
        """
        self.telescope_panel.enable_controls()
        self.telescope_disconnect_action.setEnabled(True)
        self.telescope_sleep_action.setEnabled(True)
        self.telescope_wake_action.setEnabled(False)
        self.telescope_connect_action.setEnabled(False)
        self.telescope_park_action.setEnabled(True)
        # self.start_align_action.setEnabled(True)
        # self.finish_align_action.setEnabled(True)
        # self.skip_align_action.setEnabled(True)

    def closeEvent(self, event):
        print("exiting")
        self.telescope.disconnect()
        self.is_alive = False
        event.accept()
