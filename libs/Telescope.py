#! /usr/bin/env python

import time
import threading
import datetime
import random
import math
import numpy as np
from astropy.coordinates import Angle
from astropy.coordinates import SkyCoord
from astropy.time import Time
import zmq
from serial import Serial
from serial import SerialException
from PyQt5 import QtCore
from PyQt5.QtWidgets import QWidget
# from PyQt5.QtWidgets import QMessageBox
# from .Alignment import Aligner


class Telescope(QWidget):
    status_signal = QtCore.pyqtSignal(str)
    new_coordinates = QtCore.pyqtSignal(dict)
    connected_signal = QtCore.pyqtSignal()
    disconnected_signal = QtCore.pyqtSignal()
    sleep_signal = QtCore.pyqtSignal()
    wake_signal = QtCore.pyqtSignal()
    mutex = QtCore.QMutex()

    def __init__(self, main_window):
        super().__init__()
        self.main_window = main_window
        self.connected = False
        self.sleeping = False
        # We do not know the current telescope coordinates.
        self.aligned = True
        self.align_star = None
        self.current_ra = None
        self.current_dec = None
        self.current_object_name = None
        # Creating zmq socket
        self.context = zmq.Context()
        self.zmq_socket = self.context.socket(zmq.PAIR)
        self.zmq_socket.setsockopt(zmq.RCVTIMEO, 3000)
        self.zmq_socket.connect("tcp://localhost:5556")

    def connect(self):
        if self.connected is False:
            threading.Thread(target=self.make_connection).start()

    def connect_dummy(self):
        if self.connected is False:
            threading.Thread(target=self.make_dummy_connection).start()

    def disconnect(self):
        if self.connected is True:
            self.halt()
            self.device.close()
            self.status_signal.emit("Disconnected")
            self.connected = False
            self.disconnected_signal.emit()

    def make_connection(self):
        self.status_signal.emit("Connecting")
        while 1:
            try:
                self.device = Serial("COM1", baudrate=9600, timeout=0.5)
                # We have opened the com port, but this does not mean that the
                # telescope is working now. Let's send some command to see if we are
                # indeed communicating with the telescope
                response = self.send_command(":GC#", get_response=True)
                if len(response) == 0:
                    self.device.close()
                    continue
                self.connected = True
                break
            except SerialException:
                time.sleep(1)
        # self.set_initial_date_and_time()
        self.status_signal.emit("Connected")
        self.connected_signal.emit()
        threading.Thread(target=self.current_coordinates_updater).start()
        threading.Thread(target=self.listen_commands).start()

    def make_dummy_connection(self):
        """
        Connect to a dummy device in case when there is no telesope attached to the
        computer for a testing purpose
        """
        self.status_signal.emit("Connecting")
        time.sleep(1)
        self.device = DummyDevice()
        self.connected = True
        # self.set_initial_date_and_time()
        self.connected_signal.emit()
        self.status_signal.emit("Connected")
        threading.Thread(target=self.current_coordinates_updater).start()
        threading.Thread(target=self.listen_commands).start()

    def listen_commands(self):
        """
        Function listens for a dithering command to appear on a ZMQ socket and do
        dithering if a corresponding checkbox is set to true on the right panel of
        the main window
        """
        while self.connected:
            try:
                msg = self.zmq_socket.recv().decode()
                print("Got new messange:", msg)
            except zmq.error.Again:
                time.sleep(0.05)
                continue
            if msg == "dither":
                # Got a dithering command
                if self.main_window.telescope_panel.dithering_checkbox.checkState() == 2:
                    self.dither()
            elif msg.startswith("mv"):
                # Got move command
                dx = int(msg.split(":")[1].split("|")[0])
                dy = int(msg.split(":")[1].split("|")[1])
                self.recenter(dx, dy)

    def set_initial_date_and_time(self):
        """
        :hIYYMMDDHHMMSS#
        Bypass handbox entry of daylight savings, date and time. Use the values supplied in
        this command. This feature isintended to allow use of the Autostar II from permanent
        installations where GPS reception is not possible, such as withinmetal domes.  This
        command must be issued while the telescope is waiting at the initial daylight savings
        prompt.Returns: 1 – if command was accepted.
        """
        localtime = datetime.datetime.now()
        hour = localtime.hour + 3  # Convert UTC that is set on the telescope PC to localtime
        minute = localtime.minute
        second = localtime.second
        day = localtime.day
        month = localtime.month
        year = localtime.year
        command = ":hI"
        command += "%s%02i%02i" % (str(year)[-2:], month, day)
        command += "%02i%02i%02i#" % (hour, minute, second)
        self.status_signal.emit("Setting time")
        self.send_command(command, True)

    def send_command(self, command, get_response=False):
        # self.device.flushOutput()
        # Do not sent commands if sleeping unless it's a wake command
        if self.sleeping and command != ":hW#":
            return
        self.mutex.lock()  # Prevent several commands to be sent simultaneously
        self.device.read(self.device.out_waiting)
        self.device.write(str.encode(command))
        if get_response is True:
            response = []
            while 1:
                new_byte = self.device.read(size=1).decode("latin_1")
                if (new_byte == "#") or (new_byte == ""):
                    break
                response.append(new_byte)
            response = "".join(response)
        else:
            response = ""
        self.mutex.unlock()
        return response

    # def start_alignment(self):
    #     message = "Begin alignment? (Don't forget to put the telescope"
    #     message += "in the polar position before starting the alignment)."
    #     reply = QMessageBox.question(self, 'Alignment', message,
    #                                  QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
    #     if reply != QMessageBox.Yes:
    #         # The user has canceled the alignment
    #         return
    #     # Find the best alignment star
    #     aligner = Aligner()
    #     self.align_star = aligner.find_best_star(self.main_window.site_location)
    #     # The real coordinates of the telescope are supposed to be (st, 90)
    #     real_ra = aligner.st
    #     real_dec = 90
    #     # The difference between the real and the instrumental coordinates
    #     delta_ra = 15*real_ra - self.current_ra
    #     delta_dec = real_dec - self.current_dec
    #     # Compute the coordinates of the align star in the instrumental system
    #     align_ra = self.align_star["equatorial"].ra.deg - delta_ra
    #     align_dec = self.align_star["equatorial"].dec.deg - delta_dec
    #     align_ra_angle = Angle(f"{align_ra}d")
    #     align_dec_angle = Angle(f"{align_dec}d")
    #     target_coordinates = {}
    #     target_coordinates["ra_h"] = align_ra_angle.hms.h
    #     target_coordinates["ra_m"] = align_ra_angle.hms.m
    #     target_coordinates["ra_s"] = align_ra_angle.hms.s
    #     target_coordinates["dec_d"] = align_dec_angle.dms.d
    #     target_coordinates["dec_m"] = align_dec_angle.dms.m
    #     target_coordinates["dec_s"] = align_dec_angle.dms.s
    #     self.go_to_target(target_coordinates)
    #     self.status_signal.emit("Aligning...")

    # def finish_alignment(self):
    #     if self.align_star is None:
    #         QMessageBox.question(self, 'Alignment', "You should run 'Start alignment first'",
    #                              QMessageBox.Ok, QMessageBox.Ok)
    #         return
    #     self.send_command(":Ls1#", get_response=True)
    #     self.send_command(f":LS{self.align_star['sao_number']}#")
    #     self.send_command(":CM#", get_response=True)
    #     self.aligned = True
    #     self.status_signal.emit("Connected")

    # def skip_alignment(self):
    #     reply = QMessageBox.question(self, 'Alignment', "Skip alignment?",
    #                                  QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
    #     if reply == QMessageBox.Yes:
    #         self.aligned = True
    #         self.status_signal.emit("Connected")

    def halt(self):
        """
        Stop all movement
        """
        self.send_command(":Q#")
        self.status_signal.emit("Idle")

    def guide(self, direction, reset_speed=True):
        """
        Direction can be nw, n, ne, w, e, sw, s, se
        """
        if len(direction) not in (1, 2):
            return
        if reset_speed is True:
            # Set guiding speed
            self.send_command(":RC#")
        self.status_signal.emit("Guiding %s" % (direction.upper()))
        # Starting motors
        for d in direction:
            if d not in "nesw":
                continue
            self.send_command(f":M{d}#")

    def sleep(self):
        """
        Sleep Telescope.  Power off motors, encoders, displays and lights. Scope remains
        in minimum power mode until a keystroke is received or a wake command is sent.
        """
        self.send_command(":hN#")
        self.status_signal.emit("Sleep")
        self.sleep_signal.emit()
        self.sleeping = True

    def wake(self):
        """
        Wake up sleeping telescope.
        """
        self.sleeping = False
        threading.Thread(target=self.current_coordinates_updater).start()
        self.send_command(":hW#")
        self.status_signal.emit("Idle")
        self.wake_signal.emit()

    def go_to_target(self, target_coordinates):
        # Set RA
        ra_h = int(target_coordinates["ra_h"])
        ra_m = int(target_coordinates["ra_m"])
        ra_s = int(target_coordinates["ra_s"])
        command = ":Sr%02i:%02i:%02i#" % (ra_h, ra_m, ra_s)
        self.send_command(command)
        # Set DEC
        dec_d = int(target_coordinates["dec_d"])
        dec_m = abs(int(target_coordinates["dec_m"]))
        dec_s = abs(int(target_coordinates["dec_s"]))
        command = ":Sd%+03i*%02i:%02i#" % (dec_d, dec_m, dec_s)
        self.send_command(command)
        # Slew
        command = ":MS#"
        self.send_command(command)
        # Set dither counter to zero
        self.dither_counter = 0
        # Save the target object  name
        self.current_object_name = self.main_window.object_panel.obj_name_label.text()

    def park(self):
        """Park telescope to a position A=0h h=10d"""
        # Compute parking position
        park_pos_altaz = SkyCoord(alt=10, az=180, location=self.main_window.site_location,
                                  frame="altaz", obstime=Time.now(), unit="deg")
        parkpos_equatorial = park_pos_altaz.icrs
        target_coordinates = {}
        target_coordinates["ra_h"] = int(parkpos_equatorial.ra.hms.h)
        target_coordinates["ra_m"] = int(parkpos_equatorial.ra.hms.m)
        target_coordinates["ra_s"] = int(parkpos_equatorial.ra.hms.s)
        target_coordinates["dec_d"] = int(parkpos_equatorial.dec.dms.d)
        target_coordinates["dec_m"] = int(parkpos_equatorial.dec.dms.m)
        target_coordinates["dec_s"] = int(parkpos_equatorial.dec.dms.s)
        self.go_to_target(target_coordinates)

    def current_coordinates_updater(self):
        """
        This function runs in the background and sends the telescope current coordinates
        every few seconds
        """
        while self.connected and (not self.sleeping):
            # get_telescope_ra
            ra_string = self.send_command(":GR#", True)
            try:
                ra_h, ra_m, ra_s = ra_string.split(":")
            except ValueError:
                continue
            if int(ra_h) > 24:
                continue
            self.current_ra = 15 * (int(ra_h) + int(ra_m) / 60 + int(ra_s) / 3600)
            # get_telescope_dec
            dec_string = self.send_command(":GD#", True)
            try:
                dec_d, mins_and_secs = dec_string.split(u"\xdf")
                dec_m, dec_s = mins_and_secs.split(":")
                dec_d = int(dec_d)
                dec_m = abs(int(dec_m))
                dec_s = abs(int(dec_s))
            except (ValueError, AttributeError) as e:
                continue
            if dec_d >= 0:
                self.current_dec = dec_d + dec_m / 60 + dec_s / 3600
            else:
                self.current_dec = dec_d - dec_m / 60 - dec_s / 3600
            # Send the new coordinates to all receivers
            coords = {"ra_h": ra_h, "ra_m": ra_m, "ra_s": ra_s,
                      "dec_d": dec_d, "dec_m": dec_m, "dec_s": dec_s}
            self.new_coordinates.emit(coords)
            time.sleep(2)
        print("stopped updating")

    def dither(self):
        if self.dither_counter == 0:
            # We are dithering the telescope for the first time now, so the
            # image is centered on the object and we can take current coordinates
            # as fiducial coordinates of the object around which we will wander
            self.object_ra = self.current_ra
            self.object_dec = self.current_dec
            # Save current object coordinates into a file
            fout = open("pointing.dat", "a")
            time_string = datetime.datetime.now().strftime("%d.%m.%Y;%H:%M:%S")
            fout.write("%s  %s %1.6f %1.6f\n" % (time_string, self.current_object_name, self.object_ra, self.object_dec))
            fout.close()
        # Read base step (in arcmins from right panel entry)
        try:
            base_step = float(self.main_window.telescope_panel.dithering_step_entry.text())
        except ValueError:
            base_step = 1.0
        # Find random shift around the fiducial object coordinates at which
        # we will end up after the current dithering.
        dRA_arcmin = 0.5 * random.uniform(-base_step, base_step)
        dDEC_arcmin = random.uniform(-base_step, base_step)
        dRA_deg = dRA_arcmin / (60 * math.cos(math.radians(abs(self.object_dec))))
        dDEC_deg = dDEC_arcmin / 60
        self.small_relative_shift(dRA_deg, dDEC_deg)
        self.dither_counter += 1

    def recenter(self, dRA_arcsec, dDEC_arcsec):
        # Move to a relative coordinates. Both shifts are in arcseconds
        ra_shift = dRA_arcsec / 3600
        dec_shift = dDEC_arcsec / 3600
        ra_before_shift = self.current_ra
        dec_before_shift = self.current_dec
        new_target_ra = self.current_ra + ra_shift
        new_target_dec = self.current_dec + dec_shift
        # Perform shifting
        self.small_relative_shift(ra_shift/math.cos(math.radians(abs(self.current_dec))), dec_shift)
        # # Wait for coordinates to be updated
        # time.sleep(1.75)
        # for i in range(6):
        #     time.sleep(0.25)
        #     if (self.current_ra != ra_before_shift) or (self.current_dec != dec_before_shift):
        #         # coordinates were changed
        #         break
        # else:
        #     # We have waited for too long, but the coordinates didn't change, perhaps the shift were
        #     # too small to be detected by the telescope sensors, so we can stop the recentering now
        #     return
        # # Now, when the first movement is done, let's find how far are we from the desired coordinates
        # # and move again to make a precise poinging
        # ra_shift = new_target_ra - self.current_ra
        # dec_shift = new_target_dec - self.current_dec
        # # Make an amendment to the previous shift
        # ra_before_shift = self.current_ra
        # dec_before_shift = self.current_dec
        # self.small_relative_shift(ra_shift/math.cos(math.radians(abs(self.current_dec))), dec_shift)
        time.sleep(1.75)
        for i in range(6):
            if (self.current_ra != ra_before_shift) or (self.current_dec != dec_before_shift):
                # coordinates were changed
                break
            time.sleep(0.25)
        self.object_ra = self.current_ra
        self.object_dec = self.current_dec

    def small_relative_shift(self, ra_shift, dec_shift):
        """
        Make a small shift relative to the current coordinates.
        ra_shift and dec_shift are in degrees
        """
        # Suppose we have a second for movement, so the slew rate should be
        # equal to ra_shift and dec_shift degrees per second.
        if ra_shift < 0:
            # Compensate sky rotation
            ra_shift -= 0.004
        # Send RA speed settings to the telescope mount
        self.send_command(":RA%1.5f#" % (1.333*abs(ra_shift)), False)

        # Compensate the backlash in delta
        if (self.dither_counter != 0) and (math.copysign(1, self.last_delta_change) != math.copysign(1, dec_shift)):
            # We are moving in the opposite direction in comparison with the last time
            dec_shift += math.copysign(25.0/3600, dec_shift)
        # Send DEC speed settings to the telescope mount
        self.send_command(":RE%1.5f#" % abs(dec_shift), False)
        # Start moving
        shift_direction = ""
        if dec_shift > 0:
            # Going north along DEC axis
            shift_direction += "n"
        else:
            # Going south
            shift_direction += "s"
        if ra_shift > 0:
            # Going east
            shift_direction += "e"
        else:
            # Going west
            shift_direction += "w"
        self.guide(shift_direction, reset_speed=False)
        # Wait for one second
        time.sleep(0.75)
        # Halt movement
        self.halt()
        # Compensate the backlash in alpha
        if ra_shift > 0:
            self.send_command(":RA0.008#", False)
            self.guide("w", reset_speed=False)
            time.sleep(0.25)
            self.halt()
        self.send_command(":RC#")
        self.last_delta_change = dec_shift


class DummyDevice(object):
    def __init__(self):
        self.content = ""
        self.out_waiting = 0
        self.current_ra_str = "01:00:00"
        self.current_dec_str = "60" + u"\xdf" + "00" + ":" + "00"
        self.current_ra_speed = 0.02
        self.current_dec_speed = 0.01

    def write(self, command):
        command = command.decode()
        print("Recieved command:", command)
        resp = ""
        if command == ":GD#":
            resp = self.current_dec_str
        elif command == ":GR#":
            resp = self.current_ra_str
        elif command.startswith(":Sd"):
            s = command[3:-1]
            d, ms = s.split("*")
            m, s = ms.split(":")
            self.target_dec_str = d + u"\xdf" + m + ":" + s
            print("target_dec_str", self.target_dec_str)
        elif command.startswith(":Sr"):
            self.target_ra_str = command[3:-1]
        elif command == ":MS#":
            # Slew
            threading.Thread(target=self.slew).start()
            # self.current_ra_str = self.target_ra_str
            # self.current_dec_str = self.target_dec_str
        elif command.startswith(":RA"):
            self.current_ra_speed = float(command[3:-1])
        elif command.startswith(":RE"):
            self.current_dec_speed = float(command[3:-1])
        elif command == ":Mn#":
            threading.Thread(target=self.start_motors, args=("n",)).start()
        elif command == ":Ms#":
            threading.Thread(target=self.start_motors, args=("s",)).start()
        elif command == ":Me#":
            threading.Thread(target=self.start_motors, args=("e",)).start()
        elif command == ":Mw#":
            threading.Thread(target=self.start_motors, args=("w",)).start()
        elif command == ":Q#":
            self.halted = True
        self.content += resp
        self.out_waiting += len(resp)

    def read(self, size):
        text = self.content[:size]
        self.content = self.content[size:]
        self.out_waiting -= size
        return text.encode("latin-1")

    def close(self):
        pass

    def slew(self):
        ra_start_h, ra_start_m, ra_start_s = self.current_ra_str.split(":")
        ra_start_decimal_deg = 15 * (int(ra_start_h) + int(ra_start_m)/60 + int(ra_start_s) / 3600)

        dec_start_d, dec_start_ms = self.current_dec_str.split("\xdf")
        dec_start_m, dec_start_s = dec_start_ms.split(":")
        dec_start_decimal_deg = int(dec_start_d) + int(dec_start_m)/60 + int(dec_start_s) / 3600

        ra_end_h, ra_end_m, ra_end_s = self.target_ra_str.split(":")
        ra_end_decimal_deg = 15 * (int(ra_end_h) + int(ra_end_m)/60 + int(ra_end_s) / 3600)

        dec_end_d, dec_end_ms = self.target_dec_str.split("\xdf")
        dec_end_m, dec_end_s = dec_end_ms.split(":")
        dec_end_decimal_deg = int(dec_end_d) + int(dec_end_m)/60 + int(dec_end_s) / 3600

        dec_points = np.linspace(dec_start_decimal_deg, dec_end_decimal_deg, 20)
        ra_points = np.linspace(ra_start_decimal_deg, ra_end_decimal_deg, 20)

        for ra, dec in zip(ra_points, dec_points):
            ra_angle = Angle(f"{ra}d")
            dec_angle = Angle(f"{dec}d")
            ra_h, ra_m, ra_s = ra_angle.hms
            dec_d, dec_m, dec_s = dec_angle.dms
            self.current_ra_str = f"{int(ra_h)}:{int(abs(ra_m))}:{int(abs(ra_s))}"
            self.current_dec_str = f"{int(dec_d)}"+u"\xdf" + f"{int(abs(dec_m))}:{int(abs(dec_s))}"
            time.sleep(0.5)

    def start_motors(self, direction):
        self.halted = False
        while not self.halted:
            if direction == "n":
                dec_d, dec_ms = self.current_dec_str.split("\xdf")
                dec_m, dec_s = dec_ms.split(":")
                dec = int(dec_d) + int(dec_m)/60 + int(dec_s) / 3600
                dec += self.current_dec_speed*0.25
                dec_angle = Angle(f"{dec}d")
                dec_d, dec_m, dec_s = dec_angle.dms
                self.current_dec_str = f"{int(dec_d)}"+u"\xdf" + f"{int(abs(dec_m))}:{int(abs(dec_s))}"
            if direction == "s":
                dec_d, dec_ms = self.current_dec_str.split("\xdf")
                dec_m, dec_s = dec_ms.split(":")
                dec = int(dec_d) + int(dec_m)/60 + int(dec_s) / 3600
                dec -= self.current_dec_speed*0.25
                dec_angle = Angle(f"{dec}d")
                dec_d, dec_m, dec_s = dec_angle.dms
                self.current_dec_str = f"{int(dec_d)}"+u"\xdf" + f"{int(abs(dec_m))}:{int(abs(dec_s))}"
            if direction == "w":
                ra_h, ra_m, ra_s = self.current_ra_str.split(":")
                ra = 15 * (int(ra_h) + int(ra_m)/60 + int(ra_s) / 3600)
                ra -= self.current_ra_speed * 0.25
                ra_angle = Angle(f"{ra}d")
                ra_h, ra_m, ra_s = ra_angle.hms
                self.current_ra_str = f"{int(ra_h)}:{int(abs(ra_m))}:{int(abs(ra_s))}"
            if direction == "e":
                ra_h, ra_m, ra_s = self.current_ra_str.split(":")
                ra = 15 * (int(ra_h) + int(ra_m)/60 + int(ra_s) / 3600)
                ra += self.current_ra_speed * 0.25
                ra_angle = Angle(f"{ra}d")
                ra_h, ra_m, ra_s = ra_angle.hms
                self.current_ra_str = f"{int(ra_h)}:{int(abs(ra_m))}:{int(abs(ra_s))}"
            time.sleep(0.25)
