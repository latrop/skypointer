#! /usr/bin/env python

from pathlib import Path
import shelve
import numpy as np
from astropy.coordinates import SkyCoord
from astropy.coordinates import FK5
from .SkyObject import SkyObject
from .suppl_funcs import get_decimal_year


class Stars(SkyObject):
    """
    A class for plotting stars
    """
    def __init__(self, location):
        super().__init__(location)
        hd_catalogue = Path(__file__).parent.parent / "data" / "hd_catalogue.db"
        db = shelve.open(str(hd_catalogue))
        self.all_stars = db["stars"]
        self.lim_mag = 0
        self.set_lim_mag(4)
        db.close()

    def set_lim_mag(self, lim_mag):
        """
        Set limiting magnitude to plot stars.
        mag: int
        """
        if self.lim_mag == lim_mag:
            return
        self.lim_mag = lim_mag
        ra_selected = []
        dec_selected = []
        names_selected = []
        for mag in range(-1, lim_mag+1):
            ra_selected.extend(self.all_stars[f"m_{mag}"]["ra"])
            dec_selected.extend(self.all_stars[f"m_{mag}"]["dec"])
            names_selected.extend(self.all_stars[f"m_{mag}"]["name"])
        ra_selected = np.array(ra_selected)
        dec_selected = np.array(dec_selected)
        # Select only rising stars
        delta_lim = self.location.lat.value - 90
        inds_rising = np.where(dec_selected > delta_lim)
        catalog_coords = SkyCoord(ra=ra_selected[inds_rising], dec=dec_selected[inds_rising],
                                  frame=FK5(equinox="J2000"), unit="deg")
        self.objects_names = [names_selected[i] for i in inds_rising[0]]
        self.equatorial_coords = catalog_coords.transform_to(frame=FK5(equinox="J%1.2f" % get_decimal_year()))

    def plot(self, ax):
        self.remove()
        zenith_distance = np.pi/2 - np.radians(self.horizonthal_coords.alt.deg)
        azimuth = np.radians(self.horizonthal_coords.az.deg) + np.pi
        self.inds_on_plot = np.where(zenith_distance < np.pi/2)[0]
        self.x_on_plot = zenith_distance[self.inds_on_plot] * np.sin(azimuth[self.inds_on_plot])
        self.y_on_plot = - zenith_distance[self.inds_on_plot] * np.cos(azimuth[self.inds_on_plot])
        plt_inst = ax.plot(self.x_on_plot, self.y_on_plot, "ko", markersize=1)[0]
        self.plot_instances.append(plt_inst)
