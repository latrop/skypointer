#! /usr/bin/env python

import shelve
from pathlib import Path
import numpy as np
from astropy.coordinates import SkyCoord
from .SkyObject import SkyObject


class Constellations(SkyObject):
    """
    A class for plotting constellations as lines connecting bright stars
    """
    def __init__(self, location):
        super().__init__(location)
        hd_catalogue = Path(__file__).parent.parent / "data" / "hd_catalogue.db"
        db = shelve.open(str(hd_catalogue))
        lines = db["lines"]
        ra = np.array(lines["all_ra"], dtype=float)
        dec = np.array(lines["all_dec"], dtype=float)
        self.equatorial_coords = SkyCoord(ra=ra, dec=dec, frame="fk5", unit="deg")
        db.close()

    def plot(self, ax):
        self.remove()
        zenith_distance = np.pi/2 - np.radians(self.horizonthal_coords.alt.deg)
        azimuth = np.radians(self.horizonthal_coords.az.deg) + np.pi
        bad_inds = np.where(zenith_distance > np.pi / 2)
        x = zenith_distance * np.sin(azimuth)
        y = - zenith_distance * np.cos(azimuth)
        x[bad_inds] = None
        y[bad_inds] = None
        line_instance = ax.plot(x, y, color="y")[0]
        self.plot_instances.append(line_instance)
