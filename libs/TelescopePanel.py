#! /usr/bin/env python

from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtCore import pyqtSignal
from PyQt5 import QtGui
from PyQt5.QtWidgets import QSizePolicy


class TelescopePanel(QtWidgets.QWidget):
    slew_signal = pyqtSignal(dict)

    def __init__(self, parent):
        super().__init__(parent)
        main_layout = QtWidgets.QVBoxLayout()
        panel_label = QtWidgets.QLabel("Telescope")
        panel_label.setFont(QtGui.QFont("Arial", 14, QtGui.QFont.Bold))
        panel_label.setStyleSheet("color: blue")
        main_layout.addWidget(panel_label)
        default_font = QtGui.QFont("Arial", 10)
        default_font_metric = QtGui.QFontMetrics(default_font)
        # Status layout
        self.statuses = {"Disconnected": "color: red",
                         "Connecting": "color: blue",
                         "Connected": "color: green",
                         "Idle": "color: green",
                         "Moving": "color: blue",
                         "Sleep": "color: red",
                         "Guiding NW": "color: blue",
                         "Guiding N": "color: blue",
                         "Guiding NE": "color: blue",
                         "Guiding W": "color: blue",
                         "Guiding E": "color: blue",
                         "Guiding SW": "color: blue",
                         "Guiding S": "color: blue",
                         "Guiding SE": "color: blue"}
        status_layout = QtWidgets.QHBoxLayout()
        status_layout.addWidget(QtWidgets.QLabel("Status:"))
        self.telescope_status_label = QtWidgets.QLabel("Disconnected")
        self.telescope_status_label.setStyleSheet('color: red')
        status_layout.addWidget(self.telescope_status_label, Qt.AlignLeft)
        main_layout.addLayout(status_layout)

        # Current coordinates
        current_coordinates_layout = QtWidgets.QHBoxLayout()
        # Alpha layout
        alpha_label = QtWidgets.QLabel("\u03B1:")
        alpha_label.setFixedWidth(default_font_metric.width("\u03B1:"))
        current_coordinates_layout.addWidget(alpha_label, Qt.AlignLeft)
        self.alpha_current_value_label = QtWidgets.QLabel("\u2014\u02b0 \u2014\u1d50 \u2014\u02e2")
        alpha_width = default_font_metric.width("24\u02b0 00\u1d50 00\u02e2")
        self.alpha_current_value_label.setFont(default_font)
        self.alpha_current_value_label.setFixedWidth(alpha_width+5)
        current_coordinates_layout.addWidget(self.alpha_current_value_label, Qt.AlignLeft)
        # Delta layout
        delta_label = QtWidgets.QLabel("\u03B4:")
        delta_label.setFixedWidth(default_font_metric.width("\u03B4:"))
        current_coordinates_layout.addWidget(delta_label, Qt.AlignLeft)
        self.delta_current_value_label = QtWidgets.QLabel("\u2014\u00B0 \u2014' \u2014''")
        delta_width = default_font_metric.width("-90\u00B0 00' 00''")
        self.delta_current_value_label.setFont(default_font)
        self.delta_current_value_label.setFixedWidth(delta_width+5)
        current_coordinates_layout.addWidget(self.delta_current_value_label, Qt.AlignLeft)
        current_coordinates_layout.addStretch()
        main_layout.addLayout(current_coordinates_layout)

        # targeting layout. Consists of alpha and delta target coordinates entries and the Slew button
        targeting_layout = QtWidgets.QHBoxLayout()
        # Target coordinates layout
        # Create a layout
        target_coordinates_layout = QtWidgets.QVBoxLayout()
        # Alpha
        alpha_target_layout = QtWidgets.QHBoxLayout()
        lbl = QtWidgets.QLabel("\u03B1:")
        lbl.setFixedWidth(default_font_metric.width("\u03B1:"))
        alpha_target_layout.addWidget(lbl, Qt.AlignLeft)
        self.alpha_target_entry = QtWidgets.QLineEdit()
        self.alpha_target_entry.setFixedWidth(80)
        self.alpha_target_entry.setToolTip("Right ascension: hh:mm:ss")
        self.alpha_target_entry.textEdited.connect(self.alpha_changed)
        alpha_target_layout.addWidget(self.alpha_target_entry, Qt.AlignLeft)
        target_coordinates_layout.addLayout(alpha_target_layout)
        # Delta
        delta_target_layout = QtWidgets.QHBoxLayout()
        lbl = QtWidgets.QLabel("\u03B4:")
        lbl.setFixedWidth(default_font_metric.width("\u03B4:"))
        delta_target_layout.addWidget(lbl)
        self.delta_target_entry = QtWidgets.QLineEdit()
        self.delta_target_entry.setFixedWidth(80)
        self.delta_target_entry.setToolTip("Declination: dd:mm:ss")
        self.delta_target_entry.textEdited.connect(self.delta_changed)
        delta_target_layout.addWidget(self.delta_target_entry, Qt.AlignLeft)
        target_coordinates_layout.addLayout(delta_target_layout)
        targeting_layout.addLayout(target_coordinates_layout)
        # Slew button
        slew_button_layout = QtWidgets.QHBoxLayout()
        self.slew_button = QtWidgets.QPushButton("Slew")
        self.slew_button.setFixedSize(80, 60)
        self.slew_button.setFont(QtGui.QFont("Arial", 14, QtGui.QFont.Black))
        self.slew_button.clicked.connect(self.slew)
        self.slew_button.setEnabled(False)
        slew_button_layout.addWidget(self.slew_button, Qt.AlignLeft)
        slew_button_layout.addStretch()
        targeting_layout.addLayout(slew_button_layout)
        # Add the targeting layout to the main layout
        main_layout.addWidget(QtWidgets.QLabel("Target:"))
        main_layout.addLayout(targeting_layout)

        # Navigation buttons
        button_size = 55
        navigation_layout = QtWidgets.QVBoxLayout()
        # First row:
        first_row_navigation_layout = QtWidgets.QHBoxLayout()
        self.nav_NW_button = QtWidgets.QPushButton("\u2b09")
        self.nav_NW_button.setFixedSize(button_size, button_size)
        self.nav_NW_button.setFont(QtGui.QFont("Arial", 18, QtGui.QFont.Black))
        self.nav_NW_button.setEnabled(False)
        self.nav_N_button = QtWidgets.QPushButton("\u2b06")
        self.nav_N_button.setFixedSize(button_size, button_size)
        self.nav_N_button.setFont(QtGui.QFont("Arial", 18, QtGui.QFont.Black))
        self.nav_N_button.setEnabled(False)
        self.nav_NE_button = QtWidgets.QPushButton("\u2b08")
        self.nav_NE_button.setFixedSize(button_size, button_size)
        self.nav_NE_button.setFont(QtGui.QFont("Arial", 18, QtGui.QFont.Black))
        self.nav_NE_button.setEnabled(False)
        first_row_navigation_layout.addWidget(self.nav_NW_button)
        first_row_navigation_layout.addWidget(self.nav_N_button)
        first_row_navigation_layout.addWidget(self.nav_NE_button)
        first_row_navigation_layout.addStretch()
        navigation_layout.addLayout(first_row_navigation_layout)

        # Second row:
        second_row_navigation_layout = QtWidgets.QHBoxLayout()
        self.nav_W_button = QtWidgets.QPushButton("\u2b05")
        self.nav_W_button.setEnabled(False)
        self.nav_W_button.setFixedSize(button_size, button_size)
        self.nav_W_button.setFont(QtGui.QFont("Arial", 18, QtGui.QFont.Black))
        self.nav_stop_button = QtWidgets.QPushButton("\u25a0")
        self.nav_stop_button.setEnabled(False)
        self.nav_stop_button.setFixedSize(button_size, button_size)
        self.nav_stop_button.setFont(QtGui.QFont("Arial", 18, QtGui.QFont.Black))
        self.nav_E_button = QtWidgets.QPushButton("\u27a1")
        self.nav_E_button.setFixedSize(button_size, button_size)
        self.nav_E_button.setFont(QtGui.QFont("Arial", 18, QtGui.QFont.Black))
        self.nav_E_button.setEnabled(False)
        second_row_navigation_layout.addWidget(self.nav_W_button)
        second_row_navigation_layout.addWidget(self.nav_stop_button)
        second_row_navigation_layout.addWidget(self.nav_E_button)
        second_row_navigation_layout.addStretch()
        navigation_layout.addLayout(second_row_navigation_layout)

        # Third row
        third_row_navigation_layout = QtWidgets.QHBoxLayout()
        self.nav_SW_button = QtWidgets.QPushButton("\u2b0b")
        self.nav_SW_button.setFixedSize(button_size, button_size)
        self.nav_SW_button.setFont(QtGui.QFont("Arial", 18, QtGui.QFont.Black))
        self.nav_SW_button.setEnabled(False)
        self.nav_S_button = QtWidgets.QPushButton("\u2b07")
        self.nav_S_button.setFixedSize(button_size, button_size)
        self.nav_S_button.setFont(QtGui.QFont("Arial", 18, QtGui.QFont.Black))
        self.nav_S_button.setEnabled(False)
        self.nav_SE_button = QtWidgets.QPushButton("\u2b0a")
        self.nav_SE_button.setFixedSize(button_size, button_size)
        self.nav_SE_button.setFont(QtGui.QFont("Arial", 18, QtGui.QFont.Black))
        self.nav_SE_button.setEnabled(False)
        third_row_navigation_layout.addWidget(self.nav_SW_button)
        third_row_navigation_layout.addWidget(self.nav_S_button)
        third_row_navigation_layout.addWidget(self.nav_SE_button)
        third_row_navigation_layout.addStretch()
        navigation_layout.addLayout(third_row_navigation_layout)

        main_layout.addLayout(navigation_layout)

        # Dithering layout
        dithering_layout = QtWidgets.QVBoxLayout()
        self.dithering_checkbox = QtWidgets.QCheckBox("Do dithering")
        self.dithering_checkbox.setChecked(True)
        self.dithering_checkbox.setEnabled(False)
        self.dithering_checkbox.stateChanged.connect(self.dithering_checkbox_text)
        dithering_layout.addWidget(self.dithering_checkbox)
        # Step layout
        dithering_step_layout = QtWidgets.QHBoxLayout()
        dithering_step_layout.addWidget(QtWidgets.QLabel("Step:"))
        self.dithering_step_entry = QtWidgets.QLineEdit()
        self.dithering_step_entry.setEnabled(False)
        self.dithering_step_entry.setFixedWidth(50)
        self.dithering_step_entry.setToolTip("Dithering step in arcminutes.")
        self.dithering_step_entry.setText("1.0")
        self.dithering_step_entry.setValidator(QtGui.QDoubleValidator(0.0, 9, 3))
        dithering_step_layout.addWidget(self.dithering_step_entry)
        dithering_step_layout.addWidget(QtWidgets.QLabel("arcmins"))
        dithering_step_layout.addStretch()
        dithering_layout.addLayout(dithering_step_layout)

        main_layout.addLayout(dithering_layout)

        main_layout.addStretch()
        self.setLayout(main_layout)
        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

    def enable_controls(self):
        self.slew_button.setEnabled(True)
        # self.nav_NW_button.setEnabled(True)
        self.nav_N_button.setEnabled(True)
        # self.nav_NE_button.setEnabled(True)
        self.nav_W_button.setEnabled(True)
        self.nav_stop_button.setEnabled(True)
        self.nav_E_button.setEnabled(True)
        # self.nav_SW_button.setEnabled(True)
        self.nav_S_button.setEnabled(True)
        #self.nav_SE_button.setEnabled(True)
        self.dithering_checkbox.setEnabled(True)
        self.dithering_step_entry.setEnabled(True)

    def disable_controls(self):
        self.slew_button.setEnabled(False)
        # self.nav_NW_button.setEnabled(False)
        self.nav_N_button.setEnabled(False)
        # self.nav_NE_button.setEnabled(False)
        self.nav_W_button.setEnabled(False)
        self.nav_stop_button.setEnabled(False)
        self.nav_E_button.setEnabled(False)
        # self.nav_SW_button.setEnabled(False)
        self.nav_S_button.setEnabled(False)
        # self.nav_SE_button.setEnabled(False)
        self.dithering_checkbox.setEnabled(False)
        self.dithering_step_entry.setEnabled(False)

    def set_status(self, status):
        self.telescope_status_label.setText(status)
        if status in self.statuses:
            self.telescope_status_label.setStyleSheet(self.statuses[status])

    def set_target_coordinates(self, obj_params):
        ra_h = obj_params['ra_h']
        ra_m = abs(obj_params['ra_m'])
        ra_s = abs(int(obj_params['ra_s']))
        alpha_line = f"{ra_h}:{ra_m}:{ra_s}"
        self.alpha_target_entry.setText(alpha_line)
        self.alpha_changed()

        dec_d = obj_params['dec_d']
        dec_m = abs(obj_params['dec_m'])
        dec_s = abs(int(obj_params['dec_s']))
        delta_line = f"{dec_d}:{dec_m}:{dec_s}"
        self.delta_target_entry.setText(delta_line)
        self.delta_changed()

    def alpha_changed(self):
        if self.validate_alpha() is True:
            self.alpha_target_entry.setStyleSheet("background-color: rgb(255, 255, 255);")
        else:
            self.alpha_target_entry.setStyleSheet("background-color: rgb(255, 150, 150);")

    def validate_alpha(self):
        line = self.alpha_target_entry.text()
        values = line.split(":")
        if len(values) != 3:
            return False
        if not all([s.isnumeric() for s in values]):
            return False
        ra_h = int(values[0])
        if (ra_h < 0) or (ra_h > 23):
            return False
        ra_m = int(values[1])
        if (ra_m < 0) or (ra_m > 60):
            return False
        ra_s = int(values[2])
        if (ra_s < 0) or (ra_s > 60):
            return False
        return True

    def delta_changed(self):
        if self.validate_delta() is True:
            self.delta_target_entry.setStyleSheet("background-color: rgb(255, 255, 255);")
        else:
            self.delta_target_entry.setStyleSheet("background-color: rgb(255, 150, 150);")

    def validate_delta(self):
        line = self.delta_target_entry.text()
        values = line.split(":")
        if len(values) != 3:
            return False
        if not all([s.replace("-", "").isnumeric() for s in values]):
            return False
        dec_d = int(values[0])
        if (dec_d < -90) or (dec_d > 90):
            return False
        dec_m = int(values[1])
        if (dec_m < 0) or (dec_m > 60):
            return False
        dec_s = int(values[2])
        if (dec_s < 0) or (dec_s > 60):
            return False
        return True

    def slew(self):
        """
        Slew to the coordinates specified in the coordinates entries
        """
        if (self.validate_alpha() is False) or (self.validate_delta() is False):
            # Wrong values in alpha or delta
            return
        ra_line = self.alpha_target_entry.text()
        ra_h, ra_m, ra_s = ra_line.split(":")
        dec_line = self.delta_target_entry.text()
        dec_d, dec_m, dec_s = dec_line.split(":")
        coords = {"ra_h": ra_h, "ra_m": ra_m, "ra_s": ra_s,
                  "dec_d": dec_d, "dec_m": dec_m, "dec_s": dec_s}
        self.slew_signal.emit(coords)
        self.dithering_checkbox.setChecked(True)

    def set_current_coordinates(self, coords, telescope_aligned=False):
        """
        Function recieves the current telescope coordinate from the telescope device and
        sets them as a labels on the telescope panel
        """
        ra_h = coords['ra_h']
        ra_m = coords['ra_m']
        ra_s = coords['ra_s']
        alpha_line = f"{ra_h}\u02b0 {ra_m}\u1d50 {ra_s}\u02e2"
        self.alpha_current_value_label.setText(alpha_line)

        dec_d = coords['dec_d']
        dec_m = coords['dec_m']
        dec_s = coords['dec_s']
        delta_line = f"{dec_d}\u00B0 {dec_m}' {dec_s}''"
        self.delta_current_value_label.setText(delta_line)

    def dithering_checkbox_text(self, state):
        """
        Change the text of the dithering checkbox according to the actual state
        """
        if state == Qt.Checked:
            self.dithering_checkbox.setText('Do dithering')
        else:
            self.dithering_checkbox.setText('Do NOT dithering!')
